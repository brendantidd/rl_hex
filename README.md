# README #

### What is this repository for? ###

* Curriculum  learning code for guided curriculum learning 
* RL for DSH1. The algorithm used is Proximal Policy Optimisation (PPO), adapted from https://github.com/openai/baselines


### Curriculum Info: ###
- Curriculum is evaluated every episode. If the curriculum is reached (3 successes in a row) the current curriculum is adjusted. This exists in the reset function of the environment: i.e. def reset() in env_pb_biped.py
- Three parts:
1. Guide forces: variable = env.Kp (range = 400 - 0), default == 400 (if python3 run.py --cur)
2. Terrain difficulty: variable = env.difficulty (range = 1-10), default = 1
3. Perturbations: variable = env.max_disturbance (range = 50 - 2000), default == 50


### How do I get set up? ###

#### Dependencies: ####
- pybullet (for pybullet sim) </br>
- tensorflow==1.14  </br>
- baselines (from https://github.com/openai/baselines) </br>
- Probably others.. </br>

#### To train: ####

`mpirun -np 16 --oversubscribe python3 run.py --cur --exp my_test --folder test_folder` </br>
- However many cores you want to use, I use 16 
- will save tensorboard plots etc to /home/User/results/rl_hex/latest/test_folder/my_test</br>

#### During training: ####
* Display while training (run at anytime, will show training in pybullet):</br>
 `python3 display.py --exp test_folder/my_test`</br>
* Tensorboard:</br>
`tensorboard --logdir /home/User/results/rl_hex/latest/test_folder` </br>

#### To test: ####
`python3 run.py --exp my_test --folder test_folder --test_pol`</br>

#### Useful flags ####
- --render render world
- --display_im, see what the robot sees
- --difficulty Manually set the initial difficulty
- --initial_disturbance 100 set the initial disturbance
- --obstacle_type : choose which obstacle 'mix' will do a combination
